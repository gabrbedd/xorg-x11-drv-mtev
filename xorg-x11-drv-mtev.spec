Name:       xorg-x11-drv-mtev
Summary:    Multitouch input driver for Xserver
Version:    0.1.13
Release:    10.0
Group:      System/X Hardware Support
License:    GPLv2
URL:        http://gitorious.org/xorg/xf86-input-mtev
Source0:    %{name}-%{version}.tar.gz
Source1:    60-cando-mtev.conf
Source2:    70-sitronix-mtev.conf
Source3:    80-Hanvon-mtev.conf
Source4:    90-ILI-mtev.conf
Patch10:    xorg-x11-drv-mtev-0.1.13-update-code-for-xserver-1.11.patch
Patch20:    xorg-x11-drv-mtev-0.1.13-add-right-mouse-button-emulation.patch
BuildRequires:  pkgconfig(xorg-server)
BuildRequires:  pkgconfig(xkbfile)
BuildRequires:  pkgconfig(xproto)
BuildRequires:  pkgconfig(inputproto)
BuildRequires:  pkgconfig(xrandr)
BuildRequires:  pkgconfig(randrproto)
BuildRequires:  pkgconfig(xextproto)
BuildRequires:  pkgconfig(mtdev)


%description
This is a xserver-xorg input driver for devices supporting Linux Multi-Touch protocol




%prep
%setup -q -n %{name}-%{version}
%patch10 -p1
%patch20 -p1

%build
# Override module install path
make %{?jobs:-j%jobs} DLIB=%{_libdir}/xorg/modules/input

%install
rm -rf %{buildroot}
%make_install DLIB=%{_libdir}/xorg/modules/input

mkdir -p %{buildroot}/etc/X11/xorg.conf.d
install -p -m 644 %{SOURCE1} %{buildroot}/etc/X11/xorg.conf.d
install -p -m 644 %{SOURCE2} %{buildroot}/etc/X11/xorg.conf.d
install -p -m 644 %{SOURCE3} %{buildroot}/etc/X11/xorg.conf.d
install -p -m 644 %{SOURCE4} %{buildroot}/etc/X11/xorg.conf.d


%files
%defattr(-,root,root,-)
%(pkg-config xorg-server --variable=moduledir )/input/mtev.so
%config  /etc/X11/xorg.conf.d/60-cando-mtev.conf
%config  /etc/X11/xorg.conf.d/70-sitronix-mtev.conf
%config  /etc/X11/xorg.conf.d/80-Hanvon-mtev.conf
%config  /etc/X11/xorg.conf.d/90-ILI-mtev.conf

%changelog
* Mon Dec 19 2011 Gabriel M. Beddingfield <gabrbedd@gmail.com> - 0.1.13-10.0
- Patch xorg-x11-drv-mtev-0.1.13-add-right-mouse-button-emulation.patch
* Mon Dec 19 2011 Gabriel M. Beddingfield <gabrbedd@gmail.com> - 0.1.13-9.0
- Patch xorg-x11-drv-mtev-0.1.13-update-code-for-xserver-1.11.patch
  (See also https://gitorious.org/~gabrbedd/xorg/gabrbedds-xf86-input-mtev)
- Update install paths to use %_libdir (necc. for 64-bit systems)
* Tue Apr 26 2011 Austin Zhang <austin.zhang@intel.com> - 0.1.13
- Add ILITEK 2105 mtev Xorg configure file. (config part of BMC#16695)
* Sun Apr 10 2011 Austin Zhang <austin.zhang@intel.com> - 0.1.13
- Add Hanvon/PixCir mtev Xorg configure file. (BMC#16296)
* Wed Feb  9 2011 Austin Zhang <austin.zhang@intel.com> - 0.1.13
- Add sitronix mtev Xorg configure file. (part of BMC#13369)
* Fri Jan 21 2011 Ling Yue <ling.yue@intel.com> - 0.1.13
- Add cando mtev Xorg configure file. (BMC #11625)
* Mon Dec 20 2010 Ling Yue <ling.yue@intel.com> - 0.1.13
- Upgrade to 0.1.13: adapted to use libmtdev to support kernel protocol A and B
- Use spectacle BMC#11254
* Thu Aug 19 2010 Priya Vijayan <priya.vijayan@intel.com> - 0.1.8~2010819
- Initial import to MeeGo
